import 'package:flutter/material.dart';
// 导入电影列表页面
import './movie/list.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '男神专用',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      // home: MyHomePage(title: 'Flutter Demo Home Page'),
      // home: Text("kui")
      home: MyHome(),
    );
  }
}

class MyHome  extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final _url = 'http://e.hiphotos.baidu.com/image/pic/item/a1ec08fa513d2697e542494057fbb2fb4316d81e.jpg';
    final urlAcount = 'http://c.hiphotos.baidu.com/image/pic/item/30adcbef76094b36de8a2fe5a1cc7cd98d109d99.jpg';
    return DefaultTabController(
      length: 3,
      child: Scaffold(
      appBar: AppBar(
        title: Text('电影列表'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {},
          )
        ],
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.all(0),
            children: <Widget>[
              UserAccountsDrawerHeader(
                accountEmail: Text('283601069@qq.com'),
                accountName: Text('kui'),
                currentAccountPicture: CircleAvatar(
                  backgroundImage: NetworkImage(urlAcount),
                  ),
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(65,182,40,0.5),
                    // color: Color(0x7fdd2378),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: NetworkImage(_url)
                      )
                  ),
                ),
                ListTile(title: Text('用户反馈'),trailing: Icon(Icons.feedback),),
                ListTile(title: Text('系统设置'),trailing: Icon(Icons.settings),),
                ListTile(title: Text('我要发布'),trailing: Icon(Icons.send),),
                // 分割线 widget
                Divider(color: Colors.grey),
                ListTile(title: Text('注销'),trailing: Icon(Icons.exit_to_app),),
            ],
            ),
        ),
        bottomNavigationBar: Container(
          decoration: BoxDecoration(color: Colors.black),
          // 一版bottomBar 高度都是50
          height: 50,
          child: TabBar(
          labelStyle:TextStyle(height: 0,fontSize: 10),
          tabs: <Widget>[
            Tab(icon: Icon(Icons.movie_filter),text: "正在热映",),
            Tab(icon: Icon(Icons.movie_creation),text: "即将上映",),
            Tab(icon: Icon(Icons.local_movies),text: "Top250",),
          ],),
        ),
        body: TabBarView(
          children: <Widget>[
            MovieList(mt: 'in_theaters',),
            MovieList(mt: 'coming_soon',),
            MovieList(mt: 'top250',),
          ],
        )
    )
      );
  }
}


class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
