import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import './detail.dart';
Dio dio = new Dio();
class MovieList extends StatefulWidget {
  //固定写法
  MovieList({Key key, @required this.mt }) : super( key : key );
  //电影类型
  final String mt;
  @override
  _MovieListState createState() => _MovieListState();
}
//有状态控件，必须结合一个状态管理类，来进行实现
class _MovieListState extends State<MovieList> with AutomaticKeepAliveClientMixin{
  int page = 1;
  int pagesize = 10;
  var mlist = [];
  var totalNum = 0;
  var _msg = '加载中。。。';
  final nebulasbaseurl = "https://explorer-backend.nebulas.io";
  final testApi = "http://www.liulongbin.top:3005/api/v2/movie";

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;

  //控件被创建时，会执行initState
  @override
  void initState() {
    super.initState();
    getMovieList();
  }

  //渲染当前 MovieList 空间的UI结构
  @override
  Widget build(BuildContext context) {
    // return Text('这是电影列表页面---' + widget.mt + '---$_msg' );
    return ListView.builder(
      itemBuilder: (BuildContext ctx,int i) {
        var mitem = mlist[i];
        return GestureDetector(
          onTap: () {
            // print('clicked');
            Navigator.push(context,MaterialPageRoute(builder: (BuildContext ctx) {
              return MovieDetail(id: mitem['id'], title: mitem['title'],);
            }));
          },
          child: Container(
          height: 200,
          decoration: BoxDecoration(color: Colors.white24 , border: Border(top: BorderSide(color: Colors.black12)) ),
          child: Row(children: <Widget>[
          Image.network(mitem['images']['small'],width: 130,height: 180,fit: BoxFit.cover),
          Container(
            padding: EdgeInsets.only(left: 10),
            height: 200,
            child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
            Text('电影名称：${mitem['title']}'),
            Text('上映年份：${mitem['year']}年'),
            Text('电影类型：${mitem['genres'].join(',')}'),
            Text('豆瓣评分：${mitem['rating']['average']}分'),
            Text('主要演员：${mitem['title']}'),
          ],)
          )
        ],),
        ),
          );
      },
      itemCount: mlist.length,
    );
  }
  getMovieList() async {
    //偏移量公式 （page-1） * pagesize
    int offset = (page - 1) * pagesize;
    var res = await dio.get( testApi + '/${widget.mt}?start=$offset&count=$pagesize');
    var result = res.data;
    print(result);
    //为私有数据赋值，更新UI
    setState(() {
      // 通过dio返回的数据必须使用【】来访问
      // _msg = result['msg'];
      mlist =result['subjects'];
    });
  }
}